#!/usr/bin/env bash

create_doas_conf() {
  local conf_file="/etc/doas.conf"
  local permit_line="permit persist $(whoami) as root"

  echo "Setting up doas"

  if [ -f "$conf_file" ]; then
    if grep -q "$permit_line" "$conf_file"; then
      echo "The line is already present in $conf_file."
    else
      echo "$permit_line" | sudo tee -a "$conf_file"
      echo "Added the line to $conf_file."
    fi
  else
    # Create a new conf file with the line
    echo "$permit_line" | sudo tee "$conf_file"
    echo "Created $conf_file with the line."
  fi
}

# Instantly exit if a command does not run successfully
set -e

script_dir="$(dirname "$(readlink -f "$0")")"

packages="$script_dir/wsl_programs.txt"

# Check and install missing packages
if ! sudo dpkg -l $(<"$packages") >/dev/null 2>&1; then
    echo "Installing missing packages..."
    sudo apt-get update
    sudo apt-get install -y $(<"$packages")
    echo "Packages installed successfully."
fi

create_doas_conf

dotfiles_url="https://gitlab.com/suraj.ojela/config-files-stow"

# Setup dotfiles
cd "$HOME"
git clone "$dotfiles_url"
cd "$(basename "$dotfiles_url" .git)"
stow .

# Change default shell to ZSH
chsh -s $(which zsh)